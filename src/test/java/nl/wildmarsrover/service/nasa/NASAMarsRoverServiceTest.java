package nl.wildmarsrover.service.nasa;

import nl.wildmarsrover.MarsException;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.transformer.NASAResultTransformer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.env.Environment;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NASAMarsRoverServiceTest {
    @InjectMocks
    private NASAMarsRoverService service;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Environment environment;

    @Mock
    private NASAResultTransformer transformer;

    @Mock
    private MarsRoverResultTO transformedResult;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void shouldMakeASuccesfullCall() throws MarsException {
        when(environment.getProperty("nasa_api_key")).thenReturn("DEMO_KEY");
        when(environment.getProperty("NASA_GLOBAL_API_URL")).thenReturn("https://api.nasa.gov/mars-photos/");
        when(transformer.convert(any())).thenReturn(transformedResult);
        service.getSearchResult(1000, MarsCamera.CHEMCAM);
    }

    @Test
    public void shouldFailOnWrongApiKey() throws MarsException {
        when(environment.getProperty("nasa_api_key")).thenReturn("DEMO_KEY_NOT_WORKING");
        when(environment.getProperty("NASA_GLOBAL_API_URL")).thenReturn("https://api.nasa.gov/mars-photos/");
        when(transformer.convert(any())).thenReturn(transformedResult);
        expectedException.expect(MarsException.class);
        service.getSearchResult(1000, MarsCamera.CHEMCAM);
    }
}
