package nl.wildmarsrover.transformer;


import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.service.nasa.NASAPhotoType;
import nl.wildmarsrover.service.nasa.NASARover;
import nl.wildmarsrover.service.nasa.NASASearchResultType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class NASAResultTransformerTest {

    @InjectMocks
    private NASAResultTransformer transformer;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void shouldConvertAndMapCorrectly() {
        NASASearchResultType source = new NASASearchResultType();
        NASAPhotoType photo = new NASAPhotoType();
        photo.setId(1l);
        photo.setImg_src("imageurl");
        photo.setSol(1000);
        NASARover rover = new NASARover();
        rover.setMax_date("2019-09-28");
        rover.setMax_sol(2000);
        photo.setRover(rover);
        source.getPhotos().add(photo);
        MarsRoverResultTO result = transformer.convert(source);
        assertEquals(result.getPhotos().get(0).getImgSrc(), photo.getImg_src());
        assertEquals(result.getPhotos().get(0).getMaxDate(), photo.getRover().getMax_date());
        assertEquals(result.getPhotos().get(0).getMaxSol(), photo.getRover().getMax_sol());
    }
}
