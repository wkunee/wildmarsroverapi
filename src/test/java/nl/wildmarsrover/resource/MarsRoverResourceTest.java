package nl.wildmarsrover.resource;

import nl.wildmarsrover.MarsException;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.service.SearchService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MarsRoverResourceTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private MarsRoverResource resource;

    @Mock
    private SearchService searchService;

    @Mock
    private MarsRoverResultTO result;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void shouldSucceed() {
        when(searchService.getSearchResult(1000, MarsCamera.CHEMCAM,1)).thenReturn(result);
        resource.getSearchResult(1000, MarsCamera.CHEMCAM.name(), 1);
    }

    @Test
    public void shouldFailOnNotEmptyOrWrongCamName() {
        when(searchService.getSearchResult(1000, MarsCamera.CHEMCAM,1)).thenReturn(result);
        expectedException.expect(MarsException.class);
        resource.getSearchResult(1000, "wrong", 1);
    }
}
