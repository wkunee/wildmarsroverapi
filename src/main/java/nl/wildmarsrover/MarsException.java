package nl.wildmarsrover;

public class MarsException extends RuntimeException {
    public MarsException(String message) {
        super(message);
    }

    public MarsException(String message, Exception exception) {
        super(message, exception);
    }
}
