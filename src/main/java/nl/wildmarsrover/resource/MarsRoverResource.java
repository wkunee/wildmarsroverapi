package nl.wildmarsrover.resource;

import lombok.extern.slf4j.Slf4j;
import nl.wildmarsrover.MarsException;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MarsRoverResource {

    @Autowired
    private SearchService searchService;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/api/search", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarsRoverResultTO getSearchResult(@NonNull @RequestParam(name = "sol") Integer sol, @NonNull @RequestParam(name = "cam") String cam, @NonNull @RequestParam(name = "page") Integer page) {

        if (MarsCamera.EMPTY.equals(MarsCamera.toMarsCamera(cam))) {
            throw new MarsException("Camera not found");
        }

        return searchService.getSearchResult(sol, MarsCamera.toMarsCamera(cam), page);

    }
}
