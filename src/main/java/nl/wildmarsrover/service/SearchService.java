package nl.wildmarsrover.service;

import lombok.extern.slf4j.Slf4j;
import nl.wildmarsrover.aop.ExecutionTime;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.service.nasa.NASAMarsRoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SearchService {

    @Autowired
    private NASAMarsRoverService nasaMarsRoverService;

    @ExecutionTime
    public MarsRoverResultTO getSearchResult(@NonNull Integer sol, @NonNull MarsCamera camera, @NonNull Integer page) {
        /**
         * TODO: handle pagination
         */
        return nasaMarsRoverService.getSearchResult(sol, camera);

    }
}
