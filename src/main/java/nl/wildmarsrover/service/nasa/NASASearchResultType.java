package nl.wildmarsrover.service.nasa;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NASASearchResultType {
    private List<NASAPhotoType> photos = new ArrayList<>();
}
