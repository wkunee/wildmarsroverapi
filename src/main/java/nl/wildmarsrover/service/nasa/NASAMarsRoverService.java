package nl.wildmarsrover.service.nasa;

import lombok.extern.slf4j.Slf4j;
import nl.wildmarsrover.MarsException;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.transformer.NASAResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static nl.wildmarsrover.WildMarsRoverAPIConstants.*;

@Slf4j
@Service
public class NASAMarsRoverService {
    @Autowired
    private Environment environment;

    @Autowired
    private NASAResultTransformer transformer;


    @Cacheable(cacheNames = "getSearchResult")
    public MarsRoverResultTO getSearchResult(@NonNull Integer sol, @NonNull MarsCamera camera)  {

        MarsRoverResultTO result;
        try {
            String apiKey = environment.getProperty("nasa_api_key");
            String globalApiUrl = environment.getProperty("NASA_GLOBAL_API_URL");

            StringBuilder builder = new StringBuilder();
            builder.append(globalApiUrl);
            builder.append(NASA_MARS_ROVERS_URL);
            builder.append(NASA_FIRST_MARS_ROVER_QUERY);
            builder.append(sol);
            builder.append("&camera=");
            builder.append(camera);
            builder.append("&api_key=");
            builder.append(apiKey);

            URI uri = new URI(builder.toString());

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<NASASearchResultType> resultEntity = restTemplate.getForEntity(uri, NASASearchResultType.class);

            if (resultEntity.getBody() != null) {
                result = transformer.convert(resultEntity.getBody());
            } else {
                throw new MarsException("There was no resultEntity");
            }

        } catch (Exception e) {
            log.error("An error occurred while connecting to NASA: {}", e.getMessage());
            throw new MarsException("An error occurred while connecting to NASA", e);
        }
        return result;
    }

}
