package nl.wildmarsrover.service.nasa;

import lombok.Data;

@Data
public class NASAPhotoType {
    private long id;
    private int sol;
    private String img_src;
    private NASARover rover;
}
