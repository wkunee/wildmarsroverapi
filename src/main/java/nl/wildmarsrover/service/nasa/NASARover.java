package nl.wildmarsrover.service.nasa;

import lombok.Data;

@Data
public class NASARover {
    private int max_sol;
    private String max_date;
}
