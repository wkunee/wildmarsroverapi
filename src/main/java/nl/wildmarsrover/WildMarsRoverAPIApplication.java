package nl.wildmarsrover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication

public class WildMarsRoverAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(WildMarsRoverAPIApplication.class, args);
    }
}
