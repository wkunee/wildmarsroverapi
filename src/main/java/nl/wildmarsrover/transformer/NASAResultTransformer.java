package nl.wildmarsrover.transformer;

import nl.wildmarsrover.dto.MarsRoverPhotoTO;
import nl.wildmarsrover.dto.MarsRoverResultTO;
import nl.wildmarsrover.service.nasa.NASAPhotoType;
import nl.wildmarsrover.service.nasa.NASASearchResultType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class NASAResultTransformer {

    public MarsRoverResultTO convert(@NonNull NASASearchResultType source) {
        MarsRoverResultTO result = new MarsRoverResultTO();
        result.setPhotos(source.getPhotos().stream().map(this::toMarsRoverPhotoTO).collect(Collectors.toList()));
        return result;
    }

    private MarsRoverPhotoTO toMarsRoverPhotoTO(@NonNull NASAPhotoType nasaPhotoType) {
        MarsRoverPhotoTO result = new MarsRoverPhotoTO();
        result.setId(nasaPhotoType.getId());
        result.setImgSrc(nasaPhotoType.getImg_src());
        result.setMaxDate(nasaPhotoType.getRover().getMax_date());
        result.setMaxSol(nasaPhotoType.getRover().getMax_sol());
        return result;
    }

}
