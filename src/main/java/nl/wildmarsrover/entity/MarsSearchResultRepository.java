package nl.wildmarsrover.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarsSearchResultRepository extends CrudRepository<MarsSearchResult,Long> {


}
