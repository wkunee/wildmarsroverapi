package nl.wildmarsrover.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class MarsSearchResult {

    @Id
    @GeneratedValue
    private long id;
    private String methodName;
    private long responseTime;
    private LocalDateTime queryDate;

}
