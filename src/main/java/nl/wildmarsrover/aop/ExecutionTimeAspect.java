package nl.wildmarsrover.aop;

import lombok.extern.slf4j.Slf4j;
import nl.wildmarsrover.dto.MarsCamera;
import nl.wildmarsrover.entity.MarsSearchResult;
import nl.wildmarsrover.entity.MarsSearchResultRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Aspect
@Component
@Slf4j
public class ExecutionTimeAspect {

    @Autowired
    private MarsSearchResultRepository marsSearchResultRepository;

    @Around("@annotation(nl.wildmarsrover.aop.ExecutionTime) && execution(public * *(..))")
    public Object time(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long begin = System.currentTimeMillis();

        Object value;

        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
            long duration = System.currentTimeMillis() - begin;

            Object[] signatureArgs = proceedingJoinPoint.getArgs();

            MarsSearchResult searchResult = new MarsSearchResult();
            int sol = 0;
            MarsCamera camera = MarsCamera.EMPTY;
            try {
                sol = (int) signatureArgs[0];
                camera = (MarsCamera) signatureArgs[1];
            } catch (ArrayIndexOutOfBoundsException e) {
                log.error("Could not extract method arguments");
            }

            searchResult.setMethodName("sol=" + sol + ",camera=" + camera);

            log.info("{}.{} took {} ms", proceedingJoinPoint.getSignature().getDeclaringType().getSimpleName(), proceedingJoinPoint.getSignature().getName(), duration);
            searchResult.setQueryDate(LocalDateTime.now());
            searchResult.setResponseTime(duration);
            marsSearchResultRepository.save(searchResult);
        }

        return value;
    }
}
