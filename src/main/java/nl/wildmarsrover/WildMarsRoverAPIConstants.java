package nl.wildmarsrover;

public class WildMarsRoverAPIConstants {
    public static final String NASA_MARS_ROVERS_URL = "api/v1/rovers/";
    public static final String NASA_FIRST_MARS_ROVER_QUERY = "curiosity/photos?sol=";
}
