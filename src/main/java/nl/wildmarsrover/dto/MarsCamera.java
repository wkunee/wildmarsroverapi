package nl.wildmarsrover.dto;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;

@Slf4j
public enum MarsCamera {
    FHAZ, RHAZ, MAST, CHEMCAM, MAHLI, MARDI, NAVCAM, PANCAM, MINITES, EMPTY;

    public static MarsCamera toMarsCamera(@NonNull String input) {
        MarsCamera marsCamera = MarsCamera.EMPTY;
        try {
            marsCamera = MarsCamera.valueOf(input);
        } catch (IllegalArgumentException iae) {
            log.error("Marscamera with name [{}] not found!", input);
        }
        return marsCamera;
    }
}
