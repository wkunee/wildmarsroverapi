package nl.wildmarsrover.dto;

import lombok.Data;

import java.util.List;

@Data
public class MarsRoverResultTO {
    private int amountPhotos;
    private List<MarsRoverPhotoTO> photos;

    public int getAmountPhotos() {
        if (photos != null) {
            amountPhotos = photos.size();
        }
        return amountPhotos;
    }
}
