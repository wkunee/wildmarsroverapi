package nl.wildmarsrover.dto;

import lombok.Data;

@Data
public class MarsRoverPhotoTO {
    private long id;
    private int maxSol;
    private String maxDate;
    private String imgSrc;
}
